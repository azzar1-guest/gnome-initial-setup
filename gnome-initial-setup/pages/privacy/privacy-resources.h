#ifndef __RESOURCE_privacy_H__
#define __RESOURCE_privacy_H__

#include <gio/gio.h>

extern GResource *privacy_get_resource (void);
#endif
