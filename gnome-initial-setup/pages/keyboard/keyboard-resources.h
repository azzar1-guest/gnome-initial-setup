#ifndef __RESOURCE_keyboard_H__
#define __RESOURCE_keyboard_H__

#include <gio/gio.h>

extern GResource *keyboard_get_resource (void);
#endif
